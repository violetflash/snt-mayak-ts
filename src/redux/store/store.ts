import { configureStore } from '@reduxjs/toolkit';
import {
    useSelector as rawUseSelector,
    TypedUseSelectorHook,
} from 'react-redux';

import { userSlice } from '../slices/userSlice';

export const store = configureStore({
    reducer: {
        [userSlice.name]: userSlice.reducer
    }
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
export const useSelector: TypedUseSelectorHook<RootState> = rawUseSelector;
